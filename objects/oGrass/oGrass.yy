{
    "id": "ec3141de-7872-4e39-9006-227edca16210",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oGrass",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "5c7a5270-739d-4460-a98d-cf7247a1acb7",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "d474a221-28c0-4d92-8272-5ab6bc593b42",
    "visible": true
}