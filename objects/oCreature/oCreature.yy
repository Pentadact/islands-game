{
    "id": "bbb81156-9a1f-4550-85ec-47c004663862",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oCreature",
    "eventList": [
        {
            "id": "f705c3f0-028e-4aff-9ba3-918325f7e949",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "bbb81156-9a1f-4550-85ec-47c004663862"
        },
        {
            "id": "39aded60-d23a-4811-acc2-50278b1ca78c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "bbb81156-9a1f-4550-85ec-47c004663862"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "989f86e8-38f8-49d9-a263-66219b84d32b",
    "visible": true
}