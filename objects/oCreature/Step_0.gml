
var Acceleration = 0.1;
var MaxSpeed = 200 / room_speed;
var MinSpeed = 1 / room_speed;

/* Random walk 
var MaxTurnSpeed = 1;
var TurnAcceleration = 0.2;
TurnSpeed += random_range(-TurnAcceleration,TurnAcceleration)
TurnSpeed = clamp(TurnSpeed,-MaxTurnSpeed,MaxTurnSpeed)
direction += TurnSpeed
*/

/* Avoid other creatures */
var MaxTurnSpeed = 3;
var SillyOffset = 999999;
x += SillyOffset
var NearestCreature = instance_nearest(x-SillyOffset,y,oCreature);
x -= SillyOffset
var CreatureDistance = room_width;
if instance_exists(NearestCreature) {
	DesiredDirection = point_direction(NearestCreature.x,NearestCreature.y,x,y);
	CreatureDistance = point_distance(NearestCreature.x,NearestCreature.y,x,y);
}

var DesiredCreatureDistance = 40;
var FearFactor = DesiredCreatureDistance / CreatureDistance;
if FearFactor > 1 {
	speed = MaxSpeed
} else {
	speed = lerp(MinSpeed,MaxSpeed,FearFactor)
}


//Check within bounds
global.IslandLeft = 100
global.IslandRight = room_width - 100
global.IslandTop = 100
global.IslandBottom = room_height - 100
if x < global.IslandLeft or y < global.IslandTop or x > global.IslandRight or y > global.IslandBottom {
	DesiredDirection = point_direction(x,y,room_width/2,room_height/2)
}

//Adjust direction
var DesiredDirectionChange = angle_difference(DesiredDirection,direction);
var DirectionChange = min(MaxTurnSpeed,abs(DesiredDirectionChange)) * sign(DesiredDirectionChange);
direction += DirectionChange

/* random speed change
speed += random_range(-Acceleration,Acceleration)
speed = clamp(speed,0,MaxSpeed)
*/

image_angle = direction