{
    "id": "ecbd94da-ded4-459e-a6b0-a85323ab2119",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oSand",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "5c7a5270-739d-4460-a98d-cf7247a1acb7",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "037c4d24-3f9f-41c0-8292-21ac9de8d1da",
    "visible": true
}