{
    "id": "037c4d24-3f9f-41c0-8292-21ac9de8d1da",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSand",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 236,
    "bbox_left": 27,
    "bbox_right": 228,
    "bbox_top": 23,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5aa0d4de-e12e-4857-a75e-c84b20e595b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "037c4d24-3f9f-41c0-8292-21ac9de8d1da",
            "compositeImage": {
                "id": "35a591a7-507c-43f2-9f43-212f44e87955",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5aa0d4de-e12e-4857-a75e-c84b20e595b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09a976d1-00b2-481f-884b-a1bbdaf3d036",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5aa0d4de-e12e-4857-a75e-c84b20e595b8",
                    "LayerId": "b735e10d-3195-4bf1-95b9-ac8c14124213"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "b735e10d-3195-4bf1-95b9-ac8c14124213",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "037c4d24-3f9f-41c0-8292-21ac9de8d1da",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 128
}