{
    "id": "3c8e0b24-eb00-4ff5-a2e4-fde05979e83a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sIsland",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 767,
    "bbox_left": 0,
    "bbox_right": 1023,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "18aec9d2-5ffc-4f6d-839e-1fe562a7c353",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c8e0b24-eb00-4ff5-a2e4-fde05979e83a",
            "compositeImage": {
                "id": "dc1fe8f1-12ae-4234-9df6-5d4760b262ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "18aec9d2-5ffc-4f6d-839e-1fe562a7c353",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "620c5917-81eb-4014-9c37-b1610e640919",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18aec9d2-5ffc-4f6d-839e-1fe562a7c353",
                    "LayerId": "420c4b45-3c5a-4e55-8c6b-2aa3253afd01"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 768,
    "layers": [
        {
            "id": "420c4b45-3c5a-4e55-8c6b-2aa3253afd01",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3c8e0b24-eb00-4ff5-a2e4-fde05979e83a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": 0,
    "yorig": 0
}