{
    "id": "989f86e8-38f8-49d9-a263-66219b84d32b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayer",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 34,
    "bbox_left": 7,
    "bbox_right": 32,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6372522f-5b23-449e-a575-632b6b856999",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "989f86e8-38f8-49d9-a263-66219b84d32b",
            "compositeImage": {
                "id": "cf746843-5b74-4406-9e1f-abdf641aab26",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6372522f-5b23-449e-a575-632b6b856999",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a140f04e-7dcb-4b37-99d7-f4a6b97cdc33",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6372522f-5b23-449e-a575-632b6b856999",
                    "LayerId": "bf8a982f-b9dc-49c5-9871-d8a1d051efca"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "bf8a982f-b9dc-49c5-9871-d8a1d051efca",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "989f86e8-38f8-49d9-a263-66219b84d32b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 20,
    "yorig": 20
}