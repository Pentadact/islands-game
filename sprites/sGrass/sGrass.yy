{
    "id": "d474a221-28c0-4d92-8272-5ab6bc593b42",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGrass",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 184,
    "bbox_left": 23,
    "bbox_right": 176,
    "bbox_top": 19,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a8c6e517-f682-422f-91a9-145477a9e25f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d474a221-28c0-4d92-8272-5ab6bc593b42",
            "compositeImage": {
                "id": "409ef0d1-8942-41e0-9361-e22db50961c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8c6e517-f682-422f-91a9-145477a9e25f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89967d61-2ce4-4c44-96e8-f28c9402c5a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8c6e517-f682-422f-91a9-145477a9e25f",
                    "LayerId": "75fe2dce-16c3-480f-933e-2bceba7ef7f9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 200,
    "layers": [
        {
            "id": "75fe2dce-16c3-480f-933e-2bceba7ef7f9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d474a221-28c0-4d92-8272-5ab6bc593b42",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 200,
    "xorig": 34,
    "yorig": 32
}